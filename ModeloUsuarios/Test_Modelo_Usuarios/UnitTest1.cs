using NUnit.Framework;
using PracticaCursoNet;
using System;
using System.IO;
using System.Reflection;
using System.Text.Json;

namespace Test_Modelo_Usuarios
{
    public class Tests
    {
        Persistencia persistencia;
        ModeloUsuarios model1;
        ControladorUsuarios controlador;

        void LimpiarJSON()
        {
            string fileName = "fichero.json";
            System.IO.File.Delete(Persistencia.Fichero);
            Console.WriteLine(Persistencia.Fichero);
        }

        void ResetearModeloYCtrl_ConReflection()
        {
            //FieldInfo[] camposModelo = ModeloUsuarios.Instancia.GetType().GetFields(...);
            FieldInfo[] camposModelo = typeof(ModeloUsuarios).GetFields(
                BindingFlags.NonPublic | BindingFlags.Static);

            foreach (FieldInfo campo in camposModelo)
            {
                if (campo.Name.Equals("instancia"))
                {
                    Console.WriteLine("Encontrada la instancia");
                    campo.SetValue(null, null);
                }
            }
            persistencia = new Persistencia();
            model1 = ModeloUsuarios.Instancia;
            controlador = new ControladorUsuarios(model1);
        }
        void EliminarTodosUsuarios()
        {
            var lista = ModeloUsuarios.Instancia.MostrarTodos();
            for (int i = 0; i < ModeloUsuarios.Instancia.MostrarTodos().Count; i++)
            {
                ModeloUsuarios.Instancia.EliminarUno(ModeloUsuarios.Instancia.MostrarTodos()[0].Nombre);
            }
        }

        void EliminarTodos()
        {
            var lista = model1.MostrarTodos();
            while (model1.MostrarTodos().Count > 0)
            {
                var primero = model1.MostrarTodos()[0];
                model1.EliminarUno(primero.Nombre);
            }
        }

        [SetUp]
        public void Setup()
        {
            ResetearModeloYCtrl_ConReflection();
        }

        [TearDown]
        public void AlTerminarTest()
        {
            EliminarTodos();
            if ((new Random()).Next() == 3)
            {
                LimpiarJSON();
            }
        }

        [Test]
        public void Crear()
        {
            int size = controlador.LeerEjemplos().Count;
            Assert.NotNull(controlador.Crear("julen", 22, 1.82f), "No crea el usuario de manera correcta");
            Assert.NotNull(controlador.Crear("ramon", 22, 1.75f), "No crea el usuario de manera correcta");
            Assert.NotNull(controlador.Crear("tipo de incognito", 19, 1.5f), "No crea el usuario de manera correcta");


            Assert.True(controlador.Crear(null, 20, 2).Nombre == "SIN NOMBRE", "Deber�a devolver Usuario SIN NOMBRE");
            Assert.True(controlador.Crear("prueba", -2, 2).Edad == 1, "Deber�a poner la edad a 1");
            Assert.True(controlador.Crear("prueba2", 20, -7).Altura == 0.1f, "Deber�a poner la altura a 0.1f");

            Assert.Null(controlador.Crear("tipo de incognito", 19, float.NaN), "No crea el usuario de manera correcta");

            Assert.True((controlador.LeerEjemplos().Count - size) == 6, "Algo ha ido mal");


            EliminarTodosUsuarios();
        }

        [Test]
        public void Modificar()
        {
            string mensaje_defecto = "Falla en un usuario correcto";
            string mensaje_altura = "Falla con altura negativa";
            string mensaje_edad = "Falla con edad negativa";
            string mensaje_altura_nan = "Falla con altura NaN";
            string mensaje_altura_infinita = "Falla con altura infinity";
            string mensaje_nombre = "Nombre no valido";
            string mensaje_null = "El nuevo nombre es nulo";



            controlador.Crear("Prueba1", 28, 1.9f);
            controlador.Crear("Prueba2", 28, 1.9f);
            controlador.Crear("Prueba3", 28, 1.9f);
            controlador.Crear("Prueba4", 28, 1.9f);
            controlador.Crear("Prueba5", 28, 1.9f);
            controlador.Crear("Prueba6", 28, 1.9f);
            controlador.Crear("Prueba7", 28, 1.9f);
            controlador.Crear("Prueba8", 28, 1.9f);
            controlador.Crear("Prueba9", 28, 1.9f);


            int size = controlador.LeerEjemplos().Count;

            Assert.Multiple(() =>
            {
                Assert.IsNotNull(controlador.Modificar("Prueba1", "nuevaPrueba", 32, 1.8f), mensaje_defecto);
                Assert.IsNotNull(controlador.LeerUno("nuevaPrueba"));


                Assert.IsNotNull(controlador.Modificar("Prueba2", "Prueba2", 32, -1.8f), mensaje_altura);


                Assert.IsNotNull(controlador.Modificar("Prueba3", "", 32, 1.8f), mensaje_nombre);

                Assert.IsNotNull(controlador.Modificar("Prueba4", null, 32, 1.8f), mensaje_null);

                Assert.IsNotNull(controlador.Modificar("Prueba5", "Prueba5", -32, 1.8f), mensaje_edad);

                Assert.True(controlador.Modificar("Prueba6", "Prueba6", -32, 2).Edad == 1, "Deber�a poner la edad a 1");

                Assert.Null(controlador.Modificar("Prueba7", "Prueba7", 32, float.NaN), mensaje_altura_nan);


                Assert.Null(controlador.Modificar("Prueba8", "Prueba8", 4, float.PositiveInfinity));


                Assert.IsNotNull(controlador.Modificar("Prueba9", "Prueba9", int.MaxValue, 1.8f), "Falla con Edad m�xima");

                Assert.IsNull(controlador.Modificar("Prueba11", "Prueba11", 20, 1.8f), "Encuentra uno que no existe");


                Assert.Zero(controlador.LeerEjemplos().Count - size, "El n�mero de elementos ha cambiado");

            });

            // Assert.Pass();
        }

        [Test]
        public void Eliminar()
        {
            controlador.Crear("Ram�n", 22, 1.78f);
            controlador.Crear("Sergio", 25, 1.80f);
            controlador.Crear("Laura", 21, 1.75f);

            int cantidad = controlador.LeerEjemplos().Count;

            bool eliminado = controlador.EliminarUno("Ram�n");

            if (eliminado)
            {
                Assert.IsNull(controlador.LeerUno("Ram�n"), "No se ha eliminado");
            }

            if (controlador.EliminarUno("Sergio"))
            {
                Assert.Contains(controlador.LeerUno("Laura"), (System.Collections.ICollection)controlador.LeerEjemplos());
                Assert.IsTrue(cantidad > controlador.LeerEjemplos().Count, "No se ha eliminado");
            }

        }


        [Test]
        public void Lectura()
        {
            controlador.Crear("Laura", 12, 1.75f);

            Assert.True(controlador.LeerUno("Laura").Altura == 1.75f);

        }

        [Test]
        public void PruebaGuardado()
        {
            ResetearModeloYCtrl_ConReflection();

            Assert.True(controlador.LeerEjemplos().Count == 0);
        }
    }
}