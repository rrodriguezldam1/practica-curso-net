using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Diagnostics;
using System.IO;

namespace _07_NUnit_Selenium
{
    public class Tests
    {
        IWebDriver driver;
        IWebElement textoBusq;
        IWebElement edadBusq;
        IWebElement alturaBusq;
        IWebElement botonAdd;
        IWebElement textoValidacion;
        IWebElement botonModificar;
        IWebElement botonCancelar;
        IWebElement campoNombre;
        IWebElement campoEdad;
        IWebElement campoAltura;
        IWebElement botonPagAng;
        IWebElement botonPagJS;
        IWebElement botonTabla;
        string ruta;
        Random generador;
        Process _process;

        private void StartServer()
        {
            ruta = Path.GetFullPath("../../../../../10_AJAX_WCF_ADO_Net/UsuariosWeb_APIRest/UsuariosWeb_APIRest.csproj");
            _process = new Process
            {
                StartInfo =
                {
                FileName = @"dotnet.exe",
                //Arguments = $@"run {applicationPath}\{projectName}.csproj"
                Arguments = "run --project " + ruta
                }
            };
            _process.Start();
        }

        //private void killserver()
        //{
        //    _processkill = new process
        //    {
        //        startinfo =
        //        {
        //        filename = @"dotnet.exe",
        //        //arguments = $@"run {applicationpath}\{projectname}.csproj"
        //        arguments = "taskkill /im" + _process + "/f"
        //        }
        //};
        //_processkill.start();
        //}
            [OneTimeSetUp]
        public void InicializarClaseTest()
        {
            StartServer();
            //string firefoxPortable = "C:\\Users\\pmpcurso1\\AppData\\Local\\Mozilla Firefox\\firefox.exe";
            string firefoxPortable = "../../../../../FirefoxPortable/App/Firefox64/firefox.exe";
            if (!File.Exists(firefoxPortable))
            {
                string instalador = "../../../../../FirefoxPortable_92.0_English.paf.exe";
                if (File.Exists(instalador))
                {
                    System.Diagnostics.Process.Start(instalador);
                }
            }
            if (File.Exists(firefoxPortable))
            {
                //string rutaDriverGecko = "geckodriver.exe";
                //FirefoxBinary binarioFirefox = new FirefoxBinary(firefoxPortable);

                FirefoxDriverService geckoService = FirefoxDriverService.CreateDefaultService("./");
                geckoService.Host = "::1";


                FirefoxOptions firefoxOptions = new FirefoxOptions();
                firefoxOptions.BrowserExecutableLocation = firefoxPortable;
                firefoxOptions.AcceptInsecureCertificates = true;
                driver = new FirefoxDriver(geckoService, firefoxOptions);
            }
            //ruta = Path.GetFullPath("../../../../../Pract5 HTML5/VISTA.HTML");
            driver.Navigate().GoToUrl("http://127.0.0.1:5000/index_angular.html");
            Wait(5);
            botonPagJS = driver.FindElement(By.Id("btn-vista"));
            botonPagJS.Click();

            textoBusq = driver.FindElement(By.Id("nombre"));
            edadBusq = driver.FindElement(By.Id("edad"));
            alturaBusq = driver.FindElement(By.Id("altura"));
            botonAdd = driver.FindElement(By.Id("btn-anadir"));
            botonPagAng = driver.FindElement(By.Id("btn-angular"));
            botonTabla = driver.FindElement(By.Id("btn-tabla"));
            textoValidacion = driver.FindElement(By.Id("mensaje-validacion"));
        }

        public void buscar() {
            textoBusq = driver.FindElement(By.Id("nombre"));
            edadBusq = driver.FindElement(By.Id("edad"));
            alturaBusq = driver.FindElement(By.Id("altura"));
            botonPagAng = driver.FindElement(By.Id("btn-angular"));
            botonAdd = driver.FindElement(By.Id("btn-anadir"));
            botonTabla = driver.FindElement(By.Id("btn-tabla"));
            textoValidacion = driver.FindElement(By.Id("mensaje-validacion"));
        }

        [OneTimeTearDown]
        public void FinalizarClaseTest()
        {
            Wait(5);
            _process.Kill();
            driver.Close();
            driver.Dispose();
        }


        [SetUp]
        public void Setup()
        {
            buscar();
            LimpiarCampos();
        }

        [Test]
        public void Crear()
        {
            Wait(4);
            var tablaUsuarios = driver.FindElements(By.XPath("/html/body/div[2]/div/div/div/table/tbody/tr/td[1]"));
            int contador = tablaUsuarios.Count;
            int num = 3;
            CrearValidos(num);

            Wait(1);
            botonPagAng.Click();
            Wait(4);
            botonPagJS = driver.FindElement(By.Id("btn-vista"));
            botonPagJS.Click();
            Wait(4);

            buscar();

            tablaUsuarios = driver.FindElements(By.XPath("/html/body/div[2]/div/div/div/table/tbody/tr/td[1]"));
            Assert.True(tablaUsuarios.Count == (contador + num), "No se han generado todos los usuarios");

            Assert.True(CrearInvalidos("nombre") == "Introduzca un nombre", "Deber�a aparecer otra cosa (nombre)");
            Assert.True(CrearInvalidos("edad") == "Introduzca una edad correcta", "Deber�a aparecer otra cosa (edad)");
            Assert.True(CrearInvalidos("altura") == "Introduzca una altura", "Deber�a aparecer otra cosa (altura)");

            botonTabla.Click();
            Wait(2);
            EliminarX(num);
            Assert.True(contador == tablaUsuarios.Count - num, "No se han eliminado todos los usuarios especificados");

            Wait(2);
            botonPagAng.Click();
            Wait(4);
            botonPagJS = driver.FindElement(By.Id("btn-vista"));
            botonPagJS.Click();
            Wait(1);
        }

        void IntroducirDatos(string nombre, string edad, string altura)
        {
            textoBusq.SendKeys(nombre);
            edadBusq.SendKeys(edad);
            alturaBusq.SendKeys(altura);
            botonAdd.Click();

            LimpiarCampos();
        }
        void ModificarDatos(string nombre, string edad, string altura)
        {
            textoBusq.SendKeys(nombre);
            edadBusq.SendKeys(edad);
            alturaBusq.SendKeys(altura);
            botonModificar.Click();
            LimpiarCampos();
        }

        void CrearValidos(int num)
        {
            for (int i = 0; i < num; i++)
            {
                Wait(1);
                string nombre = "Sujeto" + (i + 1);
                string edad = "" + (i + 1);
                string altura = "" + (i * 0.2f).ToString();
                IntroducirDatos(nombre, edad, altura);
            }
        }

        string CrearInvalidos(string caso)
        {
            switch (caso)
            {
                case "nombre":
                    IntroducirDatos("", "", "");
                    return textoValidacion.Text;
                case "edad":
                    IntroducirDatos("ejemplo", "string", "1.4");

                    return textoValidacion.Text;
                case "altura":
                    IntroducirDatos("ejemplo", "1", "string");
                    return textoValidacion.Text;
                default:
                    return null;
            }
        }
        void LimpiarCampos()
        {

            textoBusq.Clear();
            edadBusq.Clear();
            alturaBusq.Clear();
        }

        void ModificarUsuario(string accion, int usuariosAntes, int usuariosDespues, string nombre = "", string edad = "", string altura = "")
        {
            generador = new Random();
            int num = generador.Next(usuariosAntes, usuariosDespues);

            driver.FindElements(By.XPath("/html/body/div[2]/div/div/div/table/tbody/tr/td[5]/span"))[num].Click();
            if (accion == "modificar")
            {
                botonModificar = driver.FindElement(By.Id("btn-modificar"));
                LimpiarCampos();

                ModificarDatos(nombre, edad, altura);

                campoNombre = driver.FindElement(By.XPath("/html/body/div[2]/div/div/div/table/tbody/tr[" + (num + 1) + "]/td[1]"));
                campoEdad = driver.FindElement(By.XPath("/html/body/div[2]/div/div/div/table/tbody/tr[" + (num + 1) + "]/td[2]"));
                campoAltura = driver.FindElement(By.XPath("/html/body/div[2]/div/div/div/table/tbody/tr[" + (num + 1) + "]/td[3]"));

            }
            else
            {
                campoNombre = driver.FindElement(By.XPath("/html/body/div[2]/div/div/div/table/tbody/tr[" + (num) + "]/td[1]"));
                botonCancelar = driver.FindElement(By.Id("btn-cancelar"));
                botonCancelar.Click();
            }
        }


        [Test]
        public void Modificar()
        {
            Wait(4);
            var usuariosAntes = driver.FindElements(By.XPath("/html/body/div[2]/div/div/div/table/tbody/tr/td[1]")).Count;
            CrearValidos(5);
            var usuariosDespues = driver.FindElements(By.XPath("/html/body/div[2]/div/div/div/table/tbody/tr/td[1]")).Count;

            ModificarUsuario("cancelar", usuariosAntes, usuariosDespues);
            Assert.AreNotEqual(campoNombre.Text, "alvaro");

            ModificarUsuario("modificar", usuariosAntes, usuariosDespues, "sujeto2", "22", "1.7");
            Assert.AreEqual(campoNombre.Text, "sujeto2");
            Wait(1);

            ModificarUsuario("modificar", usuariosAntes, usuariosDespues, "sujeto2", "20", "1.3");
            Assert.AreEqual(campoEdad.Text, "20");
            Assert.AreEqual(campoAltura.Text, "1.3");
            Wait(1);

            ModificarUsuario("modificar", usuariosAntes, usuariosDespues, "", "20", "1.3");
            Assert.AreNotEqual(campoNombre.Text, "");

            botonTabla.Click();
            Wait(2);
            EliminarX(5);
            Assert.True(usuariosAntes == usuariosDespues - 5, "No se han eliminado todos los usuarios especificados");
        }

        public void Wait(int seg, int timeOut = 60)
        {
            var wait = new WebDriverWait(driver, new TimeSpan(0, 0, 1, timeOut));
            var delay = new TimeSpan(0, 0, 0, seg);
            var timeInic = DateTime.Now;
            wait.Until(driver => (DateTime.Now - timeInic) > delay);
        }

        void RealizarAccionEliminar(int usuariosAntes, int usuariosDespues, int num) {
            IWebElement borrar = driver.FindElement(By.Id("btn-eliminar" + generador.Next(usuariosAntes, usuariosDespues)));
            borrar.Click();
            var alert_box = driver.SwitchTo().Alert();
            if (num == 1)
                alert_box.Accept();
            else
                alert_box.Dismiss();
        }

        [Test]
        public void Eliminar()
        {
            Wait(4);
            var usuariosAntes = driver.FindElements(By.XPath("/html/body/div[2]/div/div/div/table/tbody/tr/td[1]")).Count;

            CrearValidos(5);

            var usuariosDespues = driver.FindElements(By.XPath("/html/body/div[2]/div/div/div/table/tbody/tr/td[1]")).Count;

            generador = new Random();
            int num = generador.Next(2);
            botonTabla.Click();

            RealizarAccionEliminar(usuariosAntes, usuariosDespues, num);
            Wait(1);

            Assert.AreEqual(usuariosDespues, driver.FindElements(By.XPath("/html/body/div[2]/div/div/div/table/tbody/tr/td[1]")).Count + ((num == 0) ? 0 : 1));

            Wait(2);
            if (num == 1)
            {
                EliminarX(4);
            }
            else
            {
                EliminarX(5);
            }
            Wait(2);
        }

        public void EliminarX(int num)
        {
            Wait(1);
            var usuarios = driver.FindElements(By.XPath("/html/body/div[2]/div/div/div/table/tbody/tr/td[1]"));
            var cantidad = usuarios.Count;
            for (int i = usuarios.Count - 1; i >= usuarios.Count - num; i--)
            {
                Wait(1);
                IWebElement borrar = driver.FindElement(By.Id("btn-eliminar" + i));
                borrar.Click();
                var alert_box = driver.SwitchTo().Alert();
                alert_box.Accept();
                cantidad--;
            }
        }
    }
}