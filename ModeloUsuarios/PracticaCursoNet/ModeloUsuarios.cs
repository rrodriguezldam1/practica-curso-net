﻿//using Ejercicio01_Encapsulacion;
using System;
using System.Collections.Generic;
using System.Text;

namespace PracticaCursoNet
{
    
    public class ModeloUsuarios : IModeloGenerico<Usuario,int, string>
    {
        public Action<Usuario> OnCreate;
        public Action<string> OnDelete;
        public Action<string,Usuario,int> OnUpdate;
        public Func <List<Usuario>> QuieroCargarDatos;
        List<Usuario> lista;
        private static ModeloUsuarios instancia = null;

        public static ModeloUsuarios Instancia
        {
            get
            {
                if (instancia == null)
                {
                    instancia = new ModeloUsuarios();

                }
                return instancia;
            }
        }

        private ModeloUsuarios()
        {
            lista = new List<Usuario>();
        }

        public Usuario MostrarUno(string linea) {
            if (lista.Count == 0)
                lista = QuieroCargarDatos();

            //lista.Find(linea);
            foreach (Usuario user in lista)
            {
                if (user.Nombre.ToLower() == linea.ToLower()) { return user; }
            }
            return null;
        }

        public bool EliminarUno(string linea) {
            if (lista.Count == 0)
                lista = QuieroCargarDatos();

            foreach (Usuario user in lista)
            {
                if (user.Nombre.ToLower() == linea.ToLower())
                {
                    bool borrar = lista.Remove(user);
                    OnDelete(linea);
                    return borrar;
                }
            }
            return false;
        }

        public Usuario Modificar(string nombreBusq, Usuario user, int index)
        {
            if (lista.Count == 0)
                lista = QuieroCargarDatos();

            user = ValidarDatos(user);

            index = lista.FindIndex(a => a.Nombre == nombreBusq);
			if ((index != -1) && (user != null))
            {
                lista[index].Nombre = user.Nombre;
                lista[index].Edad = user.Edad;
                lista[index].Altura = user.Altura;
                OnUpdate(nombreBusq,user,index);
                return lista[index];
            }
            else {
                return null;
            }
        }

        public Usuario Crear(Usuario user)
        {
            user = ValidarDatos(user);
            if (lista.Count == 0)
                lista = QuieroCargarDatos();
            if (user != null)
            {
                lista.Add(user);
                OnCreate(user);
            }
            return (user);
        }

        public IList<Usuario> MostrarTodos()
        {
            if (lista.Count == 0)
                lista = QuieroCargarDatos();

            return lista;
        }

        public Usuario ValidarDatos(Usuario user)
        {
            if (string.IsNullOrEmpty(user.Nombre) || (user.Edad.GetType() != typeof(int)) || (user.Edad < 0) || (user.Altura.GetType() != typeof(float)) || (user.Altura < 0) || (  float.IsNaN(user.Altura)) || (user.Altura == float.PositiveInfinity))
                return null;
            else
                return user;
        }
    }
}
