﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Json;

namespace PracticaCursoNet
{
    public class Persistencia
    {
        public Persistencia()
        {
            ModeloUsuarios.Instancia.OnCreate = Crear;
            ModeloUsuarios.Instancia.OnUpdate = Modificar;
            ModeloUsuarios.Instancia.OnDelete = Eliminar;
            ModeloUsuarios.Instancia.QuieroCargarDatos = DeSerializar;
        }
        public enum Entorno
        {
            Ninguno = 0,
            Produccion = 1,
            Desarrollo = 2,
            Preproduccion = 3,
        }
        const string DIR_DATA = "ModeloUsuarios";
        static Entorno entorno = Entorno.Desarrollo;
        ModeloUsuarios modelo;

        List<Usuario> listaUsuarios = new List<Usuario>();

        static public string Directorio
        {
            get
            {
                return Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)
+ "\\" + DIR_DATA + (entorno == Entorno.Produccion ? "\\Produccion" : "\\Desarrollo");
            }
        }
        static public string Fichero
        {
            get
            {
                return Directorio + "\\" + "fichero.json";
            }

        }
        public void Crear(Usuario user)
        {
            List<Usuario> lista = DeSerializar();
            lista.Add(user);
            Serializar(lista);
        }


        public void Eliminar(string linea)
        {
            List<Usuario> lista = DeSerializar();
            foreach (Usuario user in lista)
            {
                if (user.Nombre.ToLower() == linea.ToLower())
                {
                    lista.Remove(user);
                    Serializar(lista);
                    break;
                }
            }
        }

        public void Modificar(string nombreBusq, Usuario user, int index)
        {
            List<Usuario> lista = DeSerializar();

            index = lista.FindIndex(a => a.Nombre == nombreBusq);
            if (index != -1)
            {
                lista[index].Nombre = user.Nombre;
                lista[index].Edad = user.Edad;
                lista[index].Altura = user.Altura;
                Serializar(lista);
            }
        }

        public void Serializar(List<Usuario> user)
        {
            string fileName = "fichero.json";
            string jsonString = JsonSerializer.Serialize(user);
            File.WriteAllText(fileName, jsonString);
        }
        public List<Usuario> DeSerializar()
        {
            List<Usuario> listaextra = new List<Usuario>();
            try
            {
                string texto = File.ReadAllText("fichero.json");
                List<Usuario> lista = JsonSerializer.Deserialize<List<Usuario>>(texto);
                return lista;
            }
            catch
            {

            }
            return listaextra;
        }
                
}
}
