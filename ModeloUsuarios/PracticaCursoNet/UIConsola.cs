﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PracticaCursoNet
{
    public class UIConsola
    {

        public static void PedirDatos(string nombreDato, out string varDato)
        {
            Console.WriteLine(nombreDato + "?");
            do
            {
                varDato = Console.ReadLine();
            } while (string.IsNullOrEmpty(varDato));
        }

        public static void PedirNum<Tipo>(string nombreDato, out Tipo varNum)
        {
            Console.WriteLine(nombreDato + "? ");

                if (typeof(Tipo) == typeof(int))
                {
                    int numero;
                    while (!int.TryParse(Console.ReadLine(), out numero))
                    {
                        Console.WriteLine("Introduce un valor válido: ");
                    }
                varNum = (Tipo)(object)numero;
            }
                else if (typeof(Tipo) == typeof(float))
                {
                    float numero;
                while (!float.TryParse(Console.ReadLine(), out numero))
                {
                    Console.WriteLine("Introduce un valor válido: ");
                }
                varNum = (Tipo)(object)numero;
                }
                else if (typeof(Tipo) == typeof(double))
                {
                    double numero;
                while (!double.TryParse(Console.ReadLine(), out numero))
                {
                    Console.WriteLine("Introduce una edad válida: ");
                }
                varNum = (Tipo)(object)numero;
                }
                else
                {
                    throw new FormatException("ERROR: Tipo de dato no valido");
                    /*  esValorOk = true;
                     varNum = default(Tipo);
                     Console.Error.WriteLine("ERROR: Tipo de dato no valido");*/
                    // Lanzamos adrede una excepción
                    //TODO: excepción
                    // throw new FormatException("ERROR: Tipo de dato no valido");
                }
                
        }

    }
}
