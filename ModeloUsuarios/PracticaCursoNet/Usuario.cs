﻿using PracticaCursoNet;
using System;

namespace PracticaCursoNet
{

    /* Nuevo Proyecto Ejercicio01_Encapsulacion
     *  Crear una clase Usuario en otro fichero, con nombre, edad, y altura, 
     *  pero con  las variables miembro encapsuladas como propiedades
     *  Nombre no puede ser ni null ni "". En su lugar "SIN NOMBRE"
     *  edad debe ser mayor que 0
     *  Altura mínima de 0.1F metros
     *  
     *  Crear un pequeño código para comprobar que funciona:
     *      Con casos que funcione bien y casos con valores prohibidos
     */
    public class Usuario : Object, INombrable, IEditableConsola
    {
        private string nombre;
        private int edad;
        private float altura;

        public string Nombre
        {
            get
            {
                return nombre;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                    nombre = "SIN NOMBRE";
                else
                    nombre = value;
            }
        }
        public int Edad
        {
            get
            {
                return edad;
            }
            set
            {
                edad = value <= 0 ? 1 : value;
            }
        }
        public float Altura
        {
            get
            {
                return altura;
            }
            set
            {
                altura = value <= 0.1f ? 0.1f : value;
            }
        }
        public Usuario() { }
        public Usuario(string nombre, int edad, float altura)
        {
            this.Nombre = nombre;
            this.Edad = edad;
            this.Altura = altura;
        }
        public override string ToString()
        {
            return "Usuario " + this.Nombre
                + ", " + this.Edad + " años, " + Altura + " m";
        }

        public string GetNombre()
        {
            return Nombre;
        }

        public void SetNombre(string unNombre)
        {
            this.Nombre = unNombre;
        }

        public virtual void SetDatos()
        {
            
        }

        public virtual void GetDatos()
        {
            
        }
    }
}
