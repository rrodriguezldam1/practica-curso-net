﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PracticaCursoNet
{
    public interface INombrable
    {
        string GetNombre();


        void SetNombre(string unNombre);


        string Nombre
        {
            get;
            set;
        }
    }
}
