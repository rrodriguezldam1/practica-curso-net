﻿
using PracticaCursoNet;
using System;
using System.Collections.Generic;
using System.Text;

namespace PracticaCursoNet
{
    public class Empleado : Usuario
    {
        float salario;
        Coche suCoche;

        public Coche SuCoche
        {
            get { return suCoche; }
            set { suCoche = value; }
        }

        public Empleado() : base("", 0, 0)
        {

        }

        public Empleado(string nombre, int edad, float altura, float salario)
            : base(nombre, edad, altura)
        {
            this.Salario = salario;
        }

        public float Salario
        {
            get => salario;
            set => salario = (value < 7000) ? 7000 : value;
        }

        public override string ToString()
        {
            return base.ToString().Replace("Usuario", "Empleado")
                + ", " + this.Salario + " EUR  "
                + (SuCoche == null ? "" : " Posee coche: " + SuCoche);
        }

        
        public override void GetDatos()
        {
            base.GetDatos();
            UIConsola.PedirNum("Salario", out salario);
        }
    }
}

