﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PracticaCursoNet
{
    public class VistaAzul
    {
        public VistaAzul()
        {
            ModeloUsuarios.Instancia.OnCreate += Crear;
            ModeloUsuarios.Instancia.OnUpdate += Modificar;
            ModeloUsuarios.Instancia.OnDelete += Eliminar;

        }
            public void Crear(Usuario user)
            {
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine("Usuario " + user + " creado");
                Console.ForegroundColor = ConsoleColor.White;
            }
        public void Modificar(string nombreBusq, Usuario user, int index)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Ha sido modificado: " + nombreBusq + " por "+ user);
            Console.ForegroundColor = ConsoleColor.White;
        }

        public void Eliminar(string linea)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Usuario " + linea + " eliminado");
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
    }

