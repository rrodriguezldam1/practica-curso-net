﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace PracticaCursoNet
{
    public class Coche : Object, INombrable, IEditableConsola // todas las clases heredan de object
    {
        float velocidad;
        string modelo;
        protected float precio;

        // Mejor que esto ...
        // public float GetVelocidad(){ return velocidad; }
        // Las propiedades, como son métodos, empiezan por mayus
        public float Velocidad
        // Pero como se usan como variables, van sin paréntedis
        {
            get
            {
                return velocidad;
            }
        }
        public string Modelo
        {
            get
            {
                return modelo;
            }
            set
            {
                this.modelo = value;    // value es la palabra clave de C# para designar 
                                        // el único parámetro de la función set
            }
        }

        public float Precio
        // Pero como se usan como variables, van sin paréntedis
        {
            set
            {
                if (value >= 0)
                    precio = value;
                else
                    precio = 0;
            }
        }
        // Hay una manera de hacerlo resumido:
        public string Marca
        {
            get;    // Ya crea una variable interna __marca o algo así, y la usa
            set;    // de la manera común
        }
        public string Nombre
        {
            get
            {
                return GetNombre();
            }
            set
            {
                SetNombre(value);
            }
        }

        public virtual void Acelerar()
        {
            velocidad++;
        }
        public Coche(string marca, string modelo, float precio)
        {
            Modelo = modelo;
            Precio = precio;
            Marca = marca;
        }
        public Coche()
        {
            Modelo = "";
            Precio = 0;
            Marca = "";
        }
        public override bool Equals(object obj)
        {
            // base es como this, pero con la forma del padre
            // sirve para invocar a los métodos (y variables) del padre

            if (base.Equals(obj))
                return true;
            else
            {
                Coche objCoche = (Coche)obj;
                return this.Modelo == objCoche.Modelo
                    && this.Marca.Equals(objCoche.Marca)
                    && this.precio == objCoche.precio;
            }
        }
        public override string ToString()
        {
            return "Coche " + Marca + " - " + Modelo + ". ";
        }
        public string GetNombre()
        {
            return Marca + " - " + Modelo;
        }
        /// <summary>
        /// Establece tanto marca como modelo
        /// </summary>
        /// <param name="unNombre">Recibe "Marca - Modelo"</param>
        public void SetNombre(string unNombre)
        {
            if (!string.IsNullOrEmpty(unNombre))
            {
                string[] separados = unNombre.Split("-");
                Marca = separados[0].Trim();
                if (separados.Length > 1)
                {
                    Modelo = separados[1].Trim();
                }
            }
        }

        public void SetDatos()
        {
            string marca;
            UIConsola.PedirDatos("Marca", out marca);
            Marca = marca;
            UIConsola.PedirDatos("Modelo", out modelo);
            //TODO: pedir precio.
            UIConsola.PedirNum<float>("Precio", out this.precio);
        }

        public void GetDatos()
        {
            Console.WriteLine(ToString());
            Console.WriteLine("  ** Precio: " + precio + " ** ");
        }
    }
}