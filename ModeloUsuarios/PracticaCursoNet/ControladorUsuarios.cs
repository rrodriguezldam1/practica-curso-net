﻿using PracticaCursoNet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Json;

namespace PracticaCursoNet
{
    public class ControladorUsuarios
    {
        IModeloGenerico<Usuario,int,string> modelo;

        public ControladorUsuarios(ModeloUsuarios modelo)
        {
            this.modelo = modelo;

            //modelo.onChange = (Usuario user) =>
            //{
            //    Console.WriteLine("Ha cambiado usuario " + user.ToString());
            //};
        }
        public Usuario Crear(string nombre, int edad, float altura)
        {
            return modelo.Crear(new Usuario(nombre,edad,altura));
        }
        public IList<Usuario> LeerEjemplos()
        {
            return modelo.MostrarTodos();
        }
        public Usuario LeerUno(string nombre)
        {
            return modelo.MostrarUno(nombre);
        }
        
        public bool EliminarUno(string nombre)
        {
            return modelo.EliminarUno(nombre);
        }

        public Usuario Modificar(string nombreBusq, string nuevoNombre, int nuevaEdad, float nuevaAltura)
        {
            return modelo.Modificar(nombreBusq, new Usuario(nuevoNombre, nuevaEdad, nuevaAltura), -1);
        }
    }
}
