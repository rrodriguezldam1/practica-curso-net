﻿using System;

namespace PracticaCursoNet
{
    class Program
    {
        Persistencia persistencia;
        ModeloUsuarios model1;
        VistaUsuario ve;

        Program()
        {
            persistencia = new Persistencia();
            this.model1 = ModeloUsuarios.Instancia;
            ControladorUsuarios controlador = new ControladorUsuarios(model1);
            VistaAzul vistaAzul = new VistaAzul();
            // engachamos el MVC
            this.ve = new VistaUsuario(controlador);  
        }

        static void Main(string[] args)
        {
            Program program = new Program();
            program.ve.Menu();
        }

        
    }
}
