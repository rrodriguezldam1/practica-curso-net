﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PracticaCursoNet
{
    public interface IModeloGenerico<Tipo,TipoIndice, TipoBusqueda>
    {
        static IModeloGenerico<Tipo, TipoIndice, TipoBusqueda> Instancia
        {
            get;
        }
        Tipo Crear(Tipo nuevoObj);
        IList<Tipo> MostrarTodos();

        Tipo MostrarUno(TipoBusqueda linea);

        Tipo Modificar(TipoBusqueda linea, Tipo nuevoObj, TipoIndice index);
        bool EliminarUno(TipoBusqueda linea);
    }
}
