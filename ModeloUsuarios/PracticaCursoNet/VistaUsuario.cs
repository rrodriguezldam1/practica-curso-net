﻿using PracticaCursoNet;
using System;
using System.Collections.Generic;
using System.Text;

namespace PracticaCursoNet
{
    public class VistaUsuario 
    {
        ControladorUsuarios controlador;
        public VistaUsuario(ControladorUsuarios controlador)
        {
            this.controlador = controlador;
        }
        public void Menu()
        {
            int num;
            bool seguir = true;
            while (seguir)
            {
                string linea;
                Console.WriteLine("\nMENU");
                Console.WriteLine("==========================");
                Console.WriteLine("1.- Mostrar Usuario");
                Console.WriteLine("2.- Mostrar Usuarios");
                Console.WriteLine("3.- Eliminar Usuario");
                Console.WriteLine("4.- Crear Usuario");
                Console.WriteLine("5.- Modificar Usuario");
                Console.WriteLine("6.- Salir");
                Console.WriteLine("==========================");
                linea = Console.ReadLine();
                int.TryParse(linea.Trim(), out num);
                if (num == 0)
                {
                    num = 8;
                }
                switch (num)
                {
                    case 1:
                        Console.Clear();
                        MostrarUno();
                        break;
                    case 2:
                        Console.Clear();
                        MostrarTodos();
                        break;
                    case 3:
                        Console.Clear();
                        EliminarUno();
                        break;
                    case 4:
                        Console.Clear();
                        CrearUno();
                        break;
                    case 5:
                        Console.Clear();    
                        ModificarUno();
                        break;
                    case 6:
                        Console.Clear();
                        seguir = false;
                        break;
                }
            }
        }

        public void CrearUno()
        {
            string nombre;
            int edad;
            float altura;
            UIConsola.PedirDatos("Nombre", out nombre);
            UIConsola.PedirNum("Edad", out edad);
            UIConsola.PedirNum("Altura", out altura);

            controlador.Crear(nombre,edad,altura);
        }
        public void MostrarTodos()
        {
            IEnumerable<Usuario> lista = controlador.LeerEjemplos();
            if (lista !=null)
            {
                foreach (Usuario usu in lista)
                {
                    if (usu != null)
                        Console.WriteLine(usu.ToString());

                }
            }
            
            
        }
        public void EliminarUno()
        {
            Console.WriteLine("Introduce un nombre: ");
            string nombre = Console.ReadLine();
            bool eliminado = controlador.EliminarUno(nombre);
            if (!eliminado)
                Console.WriteLine("No encontrado por " + nombre);
        }
        public void MostrarUno()
       {
            Console.WriteLine(" Introduzca el nombre a buscar");
            
            string nombre = Console.ReadLine().Trim();

            Usuario user = controlador.LeerUno(nombre);
            if (user != null)
                Console.WriteLine("Usuario " + user.ToString());
            else
                Console.WriteLine("No se encuentra el usuario " + nombre);
                
            
        }
        public void ModificarUno()
        {
            Console.WriteLine("\nIntroduce el nombre del usuario a modificar");
            string nombreBusq = Console.ReadLine();
            if (controlador.LeerUno(nombreBusq) == null)
            {
                Console.WriteLine("No se ha encontrado " + nombreBusq);
            } else
            {
                string nuevoNombre;
                UIConsola.PedirDatos("Nuevo nombre", out nuevoNombre);
                int nuevaEdad;
                UIConsola.PedirNum<int>("Nueva edad", out nuevaEdad);
                float nuevaAltura;
                UIConsola.PedirNum<float>("Nueva altura", out nuevaAltura);

                this.controlador.Modificar(nombreBusq, nuevoNombre, nuevaEdad, nuevaAltura);
            }
        }
    }
}
