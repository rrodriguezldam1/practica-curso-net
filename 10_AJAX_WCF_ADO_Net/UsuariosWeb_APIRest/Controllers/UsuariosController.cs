﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ModeloUsuarios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Usuarios_CRUD_WCF.Controllers
{
    [ApiController]
    [Route("api/users")]
    public class UsuariosController : ControllerBase
    {
        ModuloPersistenciaADO modelo;
        private readonly ILogger<UsuariosController> _logger;

        public UsuariosController(ILogger<UsuariosController> logger)
        {
            _logger = logger;
            modelo = new ModuloPersistenciaADO(Entorno.Desarrollo);
        }

        [HttpGet("{id:int}")]
        public Usuario LeerUno(int id)
        {
            if (ModeloUsuario.Instancia.LeerUno(id) != null)
                return ModeloUsuario.Instancia.LeerUno(id);
            else
            {
                throw new ArgumentException($"No hay ningún usuario con ese id: {id}.");
            }
        }

        [HttpGet("{email}")]
        public Usuario LeerUno(string email)
        {
            if (modelo.LeerUno(email) != null)
                return modelo.LeerUno(email);
            else
            {
                throw new ArgumentException($"No hay ningún usuario con ese email: {email}.");
            }
        }

        [HttpGet]
        public IList<Usuario> LeerTodos()
        {
            return ModeloUsuario.Instancia.LeerTodos();
        }

        [HttpPost]
        public Usuario Crear([FromBody] Usuario usuario)
        {
            if (!string.IsNullOrEmpty(usuario.Nombre))
            {
                ModeloUsuario.Instancia.Crear(usuario);
                return modelo.LeerUno(usuario.Nombre.ToLower().Replace(' ', '_') + "@email.es");
            }
            else
                return null;
                

        }

        [HttpPut("{id:int}")]
        public bool Modificar(int id, [FromBody] Usuario usuario)
        {
            if (ModeloUsuario.Instancia.LeerUno(id) != null)
            {
                ModeloUsuario.Instancia.modificarConId(id, usuario);
                return true;
            }
            else
            {
                throw new ArgumentException($"No hay ningún id igual a: {id}.");
                //return false;
            }
        }

        [HttpDelete("{id:int}")]
        public bool Eliminar(int id)
        {
            if (ModeloUsuario.Instancia.LeerUno(id) != null)
                return ModeloUsuario.Instancia.Eliminar(id);
            else
            {
                throw new ArgumentException($"No hay ningún id igual a: {id}.");
            }
        }
    }
}
