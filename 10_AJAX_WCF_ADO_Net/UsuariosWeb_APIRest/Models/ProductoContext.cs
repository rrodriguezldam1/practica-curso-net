﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UsuariosWeb_APIRest.Models
{
    public class ProductoContext : DbContext
    {
        public ProductoContext(DbContextOptions<ProductoContext> options) : base(options)
        {
        }
        //Corresponde a una estructura tipo set (otro tipo de
        //coleccion como List o Dicc), cuya persistencia será en base de datos
        public DbSet<Producto> Producto { get; set; }

        //Metodo tipo evento que va a ser llamado al configurarse la base de datos
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                Console.WriteLine("La bbdd no ha sido configurada");
            }
            else
            {
                Console.WriteLine("La bbdd Si ha sido configurada");
            }
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Producto>(
                entity =>
                {
                    entity.Property(e => e.Nombre)
                        .IsRequired()
                        .HasMaxLength(50)
                        .IsUnicode(false);

                    entity.Property(e => e.Precio)
                    .IsRequired()
                    .HasPrecision(9,2);
                });
        }
    }
}
