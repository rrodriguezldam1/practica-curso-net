window.onload = function () {
     vista1 = new Vista();
};

class Vista {

    constructor() {
        this.controlador1 = new Controlador(this);

        document.getElementById("btn-anadir").addEventListener("click", () => {
            let usuario = this.validarUsuario();

            if (usuario !== null)
                this.controlador1.crear(usuario);
        });

        document.getElementById("btn-cancelar").addEventListener("click", () => {

            document.getElementById("nombre").value = "";
            document.getElementById("edad").value = "";
            document.getElementById("altura").value = "";
            document.getElementById("activosi").checked = false;
            document.getElementById("activono").checked = false;


            document.getElementById("mensaje-validacion").className = "no_ver";
            document.getElementById("btn-modificar").setAttribute("style", "display:none");
            document.getElementById("btn-cancelar").setAttribute("style", "display:none");
            return;
        });
    }

    crearTabla(listausuarios) {
        console.log(listausuarios);
        document.getElementById("tbody-usuarios").innerHTML = "";
        for (let user of listausuarios) {
            if(user.activo == true){
               let filaTabla = `<tr>
               <td>${user.nombre}</td>
                <td>${user.edad}</td>
                <td>${user.altura}</td>
                <td>
                    <div class="d-inline-flex align-items-center active">
                        <div class="circle"></div>
                        <div class="ps-2">Activo</div>
                    </div>
                </td>
                <td><span id="btn-modificar${listausuarios.indexOf(user)}" class="fa fa-ellipsis-v btn"></span></td>
                <td>
                <div id="btn-eliminar${listausuarios.indexOf(user)}" class="btn p-0"> <span class="fas fa-times"></span> </div>
                </td>
                </tr>`; 

                document.getElementById("tbody-usuarios").innerHTML += filaTabla;
            }else{
                let filaTabla = `<tr>
                <td>${user.nombre}</td>
                <td>${user.edad}</td>
                <td>${user.altura}</td>
                <td>
                    <div class="d-inline-flex align-items-center waiting">
                        <div class="circle"></div>
                        <div class="ps-2">Inactivo</div>
                    </div>
                </td>
                <td><span id="btn-modificar${listausuarios.indexOf(user)}" class="fa fa-ellipsis-v btn"></span></td>
                <td>
                <div id="btn-eliminar${listausuarios.indexOf(user)}" class="btn p-0"> <span class="fas fa-times"></span> </div>
                </td>
                </tr>`;

                document.getElementById("tbody-usuarios").innerHTML += filaTabla;
            }
            
            
        }
        for (let i = 0; i < listausuarios.length; i++) {
            document.getElementById(`btn-eliminar${i}`).addEventListener("click",  () => {
                this.eliminarUsuario(listausuarios[i]);
            }, false);
            document.getElementById(`btn-modificar${i}`).addEventListener("click", () => {
                this.modificarUsuario(listausuarios[i]);
            }, false);
        }
    }

    validar(mensaje) {
        document.getElementById("mensaje-validacion").className = "ver";
        document.getElementById("mensaje-validacion").innerHTML = mensaje;
    }


    validarUsuario(id) {
        let campoNombre = document.getElementById("nombre");
        let nombre = campoNombre.value.replace(/</g, "&lt;").replace(/>/g, "&gt;");
        if (nombre == "") {
            document.getElementById("mensaje-validacion").className = "ver";
            document.getElementById("mensaje-validacion").innerHTML = "Introduzca un nombre";
            return null;
        }
        let edad = parseInt(document.getElementById("edad").value.replace(/</g, "&lt;").replace(/>/g, "&gt;"));
        if (isNaN(edad)) {
            document.getElementById("mensaje-validacion").className = "ver";
            document.getElementById("mensaje-validacion").innerHTML = "Introduzca una edad correcta";
            return null;
        }
        let altura = parseFloat(document.getElementById("altura").value.replace(/</g, "&lt;").replace(/>/g, "&gt;"));
        if (isNaN(altura)) {
            document.getElementById("mensaje-validacion").className = "ver";
            document.getElementById("mensaje-validacion").innerHTML = "Introduzca una altura";
            return null;
        }
        let activo = document.getElementById("activosi").checked;
        document.getElementById("mensaje-validacion").className = "no_ver";
        return new Usuario(nombre, edad, altura, activo,id);
    }



    modificarUsuario(usuariomodificar) {

        document.getElementById("btn-modificar").replaceWith(document.getElementById("btn-modificar").cloneNode(true));

        document.getElementById("btn-modificar").setAttribute("style","display:inline-block");
        document.getElementById("btn-cancelar").setAttribute("style","display:inline-block");


        document.getElementById("nombre").value = usuariomodificar.nombre;
        document.getElementById("edad").value = usuariomodificar.edad;
        document.getElementById("altura").value = usuariomodificar.altura;
        if(usuariomodificar.activo){
            document.getElementById("activosi").checked = true;
        }else{
            document.getElementById("activono").checked = true;
        }
        

        document.getElementById("btn-modificar").addEventListener("click", () => {

            let nuevoUsuario = this.validarUsuario(usuariomodificar.id);

            if (nuevoUsuario !== null) {
                this.controlador1.modificar(nuevoUsuario);
                document.getElementById("btn-modificar").setAttribute("style","display:none");
                document.getElementById("btn-cancelar").setAttribute("style","display:none");
            }
        });

    }

    eliminarUsuario(usuarioeliminar) {
        if (confirm('¿Desea eliminar?')) {
            // Save it!
            this.controlador1.eliminar(usuarioeliminar);
        } else {
        }
    }

}


