class Controlador {
    
    constructor(vista) {
        this.id = 0;
        //this.arrayusuarios = JSON.parse(window.localStorage.getItem("usuarios"));;
        // if(this.arrayusuarios == null || this.arrayusuarios[0] == undefined){
        //     this.arrayusuarios = [];
        // }else{
        //     if(this.arrayusuarios[this.arrayusuarios.length-1].id !== null){
        //         this.id = this.arrayusuarios[this.arrayusuarios.length-1].id+1;
        //     }
            
        // }
        this.vista = vista;
        this.arrayusuarios = [];
        this.modelo1 = new Modelo(this.arrayusuarios);
        this.arrayusuarios = this.modelo1.listaUsuarios();
        this.modelo1.mostrarTabla = (arrayUsuarios) => {vista.crearTabla(arrayUsuarios);}
        this.modelo1.mostrarTabla(this.arrayusuarios);
    }

 crear(usuario){
    this.modelo1.crearUsuario(usuario.nombre, usuario.edad,usuario.altura,usuario.activo,this.id);
    this.id++;
}

 modificar(usuariomodificar){
    this.modelo1.modificarUsuario(usuariomodificar.nombre, usuariomodificar.edad,usuariomodificar.altura,usuariomodificar.activo,usuariomodificar.id);
}

//  guardarDatosControlador(){
//     modelo1.guardarDatosModelo();
// }

 eliminar(usuarioeliminar){
    this.modelo1.eliminarUsuario(usuarioeliminar.id);
}

}