import { Component, OnInit } from '@angular/core';
import { ServicioClienteUsuarioService } from '../servicio-cliente-usuario.service';

@Component({
  selector: 'app-usuarios-activo',
  templateUrl: './usuarios-activo.component.html',
  styleUrls: ['./usuarios-activo.component.css']
})
export class UsuariosActivoComponent implements OnInit {

  usuariosActivos: number = 0;
  usuariosInactivos: number = 0;

  constructor(public srvUsu: ServicioClienteUsuarioService) {
  }

  ngOnInit(): void {
  }

}
