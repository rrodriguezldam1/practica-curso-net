import { Component, OnInit } from '@angular/core';
import { ServicioClienteUsuarioService } from '../servicio-cliente-usuario.service';

@Component({
  selector: 'app-usuarios-edad',
  templateUrl: './usuarios-edad.component.html',
  styleUrls: ['./usuarios-edad.component.css']
})
export class UsuariosEdadComponent implements OnInit {

  usuariosMayores: number = 0;
  usuariosMenores: number = 0;

  constructor(public srvUsu: ServicioClienteUsuarioService) { 
    
  }

  ngOnInit(): void {
  }

}
