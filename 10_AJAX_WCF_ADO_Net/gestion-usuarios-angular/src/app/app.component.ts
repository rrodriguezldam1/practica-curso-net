import { Component } from '@angular/core';
import { ServicioClienteUsuarioService } from './servicio-cliente-usuario.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'gestion-usuarios-angular';

  constructor(public srvUsu: ServicioClienteUsuarioService){}
}
