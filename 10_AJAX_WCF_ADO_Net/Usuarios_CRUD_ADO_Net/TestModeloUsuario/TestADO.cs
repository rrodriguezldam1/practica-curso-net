using NUnit.Framework;
using ModeloUsuarios;
using System;
using System.Reflection;
using System.Collections.Generic;

namespace TestModeloUsuario
{
    public class TestsADO { 
        ModeloUsuario modelo;
        ModuloPersistenciaADO moduloPersistenciaADO;
        Usuario usu1;

        void LimpiarBBDD()
        {
            modelo = ModeloUsuario.Instancia;
            modelo.LimpiarBBDD();
        }
        void ResetearModeloYCtrl_ConReflection ()
        {
            // FieldInfo[] camposModelo = ModeloUsuario.Instancia.GetType().GetFields(...);
            FieldInfo[] camposModelo = typeof(ModeloUsuario).GetFields(
                BindingFlags.NonPublic | BindingFlags.Static);
            foreach (FieldInfo campo in camposModelo)
            {
                if (campo.Name.Equals("instance"))
                {
                    ModeloUsuario.Instancia.ToString();
                    Console.WriteLine("Encontrado instance Modelo y eliminado");
                    campo.SetValue(null, null);
                    ModeloUsuario.Instancia.ToString();
                }
            }
            moduloPersistenciaADO = new ModuloPersistenciaADO(Entorno.Desarrollo);
            modelo = ModeloUsuario.Instancia;
            //  type.GetField("instance", BindingFlags.NonPublic | BindingFlags.Static).SetValue(null, null);
        }
        void EliminarTodosUsuarios()
        {
            // var lista = ModeloUsuario.Instancia.LeerTodos();

            while (ModeloUsuario.Instancia.LeerTodos().Count > 0)
            {
                ModeloUsuario.Instancia.Eliminar(0);
            }
            /*while (ModeloUsuario.Instancia.LeerTodos().Count > 0)
            {
                var primero = ModeloUsuario.Instancia.LeerTodos()[0];
                ModeloUsuario.Instancia.Eliminar(primero.Nombre);
            }*/
        }

        [SetUp]
        public void Setup()
        {
            moduloPersistenciaADO = new ModuloPersistenciaADO(Entorno.Desarrollo);
            modelo = ModeloUsuario.Instancia;
        }
        [TearDown]
        public void AlTerminarTest()
        {
            // ResetearModeloYCtrl_ConReflection();
            //EliminarTodosUsuarios();
            //if ((new Random()).Next(3) == 0)
               LimpiarBBDD();
        }
        
        void CrearUsuariosValidos()
        {
            usu1 = new Usuario("hola", 27, 1.5f);
            modelo.Crear(usu1);
            modelo.Crear(new Usuario("adios", 33, 2));
            modelo.Crear(new Usuario("qtal", 45, 2));
        }
           
        [Test]
        public void TestCrearValidos()
        {
            CrearUsuariosValidos();
            modelo.Crear(new Usuario("adios2", -3, 2.2f));
            modelo.Crear(new Usuario("qtal2", 400, 27));
            IList<Usuario> lista = modelo.LeerTodos();
            int num = lista.Count;
            Assert.AreEqual(1, modelo.LeerUno(lista[num-2].Id).Edad);
            Assert.AreEqual(1.0f, modelo.LeerUno(lista[num - 1].Id).Altura);
            Assert.AreEqual("hola", modelo.LeerUno(lista[num - 5].Id).Nombre);
            Assert.Null(modelo.LeerUno(num-3));
            Assert.AreEqual(modelo.LeerTodos().Count, 14, "Mal creados los usuarios");
            Assert.IsTrue(modelo.LeerUno(lista[num - 5].Id).Equals(new Usuario("hola", 27, 1.5f)));
        }

        [Test]
        public void TestCrearInvalidos()
        {
            modelo.Crear(new Usuario(null, 400, 27));
            modelo.Crear(null);
            Assert.AreEqual(modelo.LeerTodos().Count, 9, "Mal creados los usuarios");
        }

            [Test]
        public void TestModificar()
        {
            CrearUsuariosValidos();
            Usuario usu1mod = new Usuario("hola", -3, 7);//Te lo cambia aqu� directamente
            modelo.Modificar(usu1mod);
            IList<Usuario> lista = modelo.LeerTodos();
            int num = lista.Count;
            Assert.AreNotEqual(modelo.LeerUno(lista[num - 3].Id), usu1);
            Assert.AreEqual(modelo.LeerUno(lista[num - 3].Id), new Usuario("hola", 1, 1));
            Usuario usuNoExiste = new Usuario("NoExisto", 88, 88);
            int contador = modelo.LeerTodos().Count;
            modelo.Modificar(usuNoExiste);
            Assert.AreEqual(0, modelo.LeerTodos().Count - contador);

            //CrearUsuariosValidos();
            //Usuario usu1mod = new Usuario("hola", -3, 7);//Te lo cambia aqu� directamente
            //modelo.ModificarConId(16, usu1mod);
            //IList<Usuario> lista = modelo.LeerTodos();
            //int num = lista.Count;
            //Assert.AreNotEqual(modelo.LeerUno(16), usu1);
            //Assert.AreEqual(modelo.LeerUno(16), new Usuario("hola", 1, 1));
            //Usuario usuNoExiste = new Usuario("NoExisto", 88, 88);
            //int contador = modelo.LeerTodos().Count;
            //modelo.ModificarConId(17, usuNoExiste);
            //Assert.AreEqual(0, modelo.LeerTodos().Count - contador);
        }
       

        [Test]
        public void TestEliminar()
        {
            int contador = modelo.LeerTodos().Count;
            Console.WriteLine(contador);
            Usuario usu1 = new Usuario("hola", 1, 1);
            modelo.Crear(usu1);
            modelo.Crear(new Usuario("adios", 43, 2));
            modelo.Crear(new Usuario("qtal", 40, 2));
            Usuario usu = modelo.LeerTodos()[modelo.LeerTodos().Count-2];
            Assert.AreEqual(true, modelo.Eliminar((int) usu.Id));
            foreach(Usuario usuario in modelo.LeerTodos())
            {
                Assert.AreEqual(false, usuario == usu);
            }
            Assert.AreEqual(2, modelo.LeerTodos().Count-contador);
            Usuario usufinal = modelo.LeerTodos()[modelo.LeerTodos().Count-1];
            Assert.AreEqual(false, modelo.Eliminar(((int) usufinal.Id) + 5));
            Assert.AreEqual(2, modelo.LeerTodos().Count-contador);
        }

        [Test]
        public void TestLeerUno()
        {
            CrearUsuariosValidos();
            IList<Usuario> lista = modelo.LeerTodos();
            Console.WriteLine(modelo.leerTodo().Count);
            Assert.Null(modelo.LeerUno(modelo.leerTodo().Count * 2));
            Assert.NotNull(modelo.LeerUno(lista[0].Id));
        }
        
        [Test]
        public void TestLeerTodos()
        {
            Assert.AreEqual(9, modelo.LeerTodos().Count);
            modelo.Crear(new Usuario("adios", 43, 2));
            modelo.Crear(new Usuario("qtal", 40, 2));
            Assert.AreEqual(11, modelo.LeerTodos().Count);
            //modelo.Eliminar(0);
            //Assert.AreEqual(1, modelo.LeerTodos().Count);
        }
        [Test]
        public void ProbarGuardadoFichero()
        {
            TestCrearValidos();

            //ResetearModeloYCtrl_ConReflection();

            //Assert.IsTrue(modelo.LeerUno(4).Equals(new Usuario("qtal2", 400, 27)));
        }
    }

}