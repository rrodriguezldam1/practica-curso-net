class Usuario {
    constructor(nombre, edad, altura, activo, id) {
        this.nombre = nombre;
        this.edad = (edad<= 0) ? 1 : edad;
        this.altura = (altura<= 0) ? 1 : altura;
        this.activo = activo;
        this.id = id;
    }
}